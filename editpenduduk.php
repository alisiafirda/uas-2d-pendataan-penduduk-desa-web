<!DOCTYPE html>
<?php
include 'koneksi.php';
    $db = new database();

?>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="bootstrap.min.css" />
  <title>Edit Penduduk</title>
</head>

<body>
  <!-- container section start -->
    <section id="container" class="">
    <header class="header dark-bg">
      <div class="toggle-nav">
        <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i class="icon_menu"></i></div>
      </div>
    </header>
    <!--header end-->
  <!-- form edit -->
  <div class="col-lg-2"></div>
    <div class="col-lg-8">
        <h1>Edit Penduduk</h1>
        <?php
            foreach($db->edit_penduduk($_GET['nik']) as $data) {
        ?>
        <form action="proses.php?aksi=update_penduduk" method="post">
                <table class="table table-bordered">
                    <tr>
                        <td>NIK</td>
                        <td><input class="form-control" type="text" name="nik"  value="<?php echo $data['nik'] ?>" readonly ></td>
                    </tr>
                    <tr>
                        <td>Nama Penduduk</td>
                        <td><input class="form-control" type="text" name="nama" value="<?php echo $data['nama'] ?>"></td>
                    </tr>
                    <tr>
                        <td>Jenis Kelamin</td>
                        <td><select class="form-control" name="jenkel">
                                        <option value='Laki-Laki'>Laki-Laki</option>
                                        <option value='Perempuan'>Perempuan</option>
                             </select></td>
                    </tr>
                    <tr>
                        <td>Pekerjaan</td>
                        <td><input class="form-control" type="text" name="pekerjaan" value="<?php echo $data['pekerjaan'] ?>"></td>
                    </tr>
                     <tr>
                        <td>Dusun</td>
                        <td><select class="form-control" name="id_dusun">
                                        <option value='1'>Gurah</option>
                                        <option value='2'>Gampeng</option>
                                         <option value='3'>Pare</option>
                                    </select></td>
                    </tr>
                      <tr>
                        <td>Ekonomi</td>
                        <td><select class="form-control" name="id_ekonomi">
                                        <option value='e1'>Atas</option>
                                        <option value='e2'>Menengah</option>
                                         <option value='e3'>Bawah</option>
                             </select></td>
                    </tr>
                    <tr>
                        <td>Foto</td>
                        <td><input class="form-control" type="file" name="photos" value="<?php echo $data['photos'] ?>"></td>
                    </tr>
                        <td></td>
                        <td><input class="btn btn-primary" type="submit" value="Simpan"></td>
                        <td><input class="btn btn-success" type="submit" value="Batal"></td>
                    </tr>
                </table>
        </form>
        <?php 
        } ?>
    </div>

  <!-- javascripts -->
  <script src="js/jquery.js"></script>
  <script src="js/jquery-ui-1.10.4.min.js"></script>
  <script src="js/jquery-1.8.3.min.js"></script>
  <script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>
  <!-- bootstrap -->
  <script src="js/bootstrap.min.js"></script>
  <!-- nice scroll -->
  <script src="js/jquery.scrollTo.min.js"></script>
  <script src="js/jquery.nicescroll.js" type="text/javascript"></script>
  <!-- charts scripts -->
  <script src="assets/jquery-knob/js/jquery.knob.js"></script>
  <script src="js/jquery.sparkline.js" type="text/javascript"></script>
  <script src="assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
  <script src="js/owl.carousel.js"></script>
  <!-- jQuery full calendar -->
  <<script src="js/fullcalendar.min.js"></script>
    <!-- Full Google Calendar - Calendar -->
    <script src="assets/fullcalendar/fullcalendar/fullcalendar.js"></script>
    <!--script for this page only-->
    <script src="js/calendar-custom.js"></script>
    <script src="js/jquery.rateit.min.js"></script>
    <!-- custom select -->
    <script src="js/jquery.customSelect.min.js"></script>
    <script src="assets/chart-master/Chart.js"></script>

    <!--custome script for all page-->
    <script src="js/scripts.js"></script>
    <!-- custom script for this page-->
    <script src="js/sparkline-chart.js"></script>
    <script src="js/easy-pie-chart.js"></script>
    <script src="js/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="js/jquery-jvectormap-world-mill-en.js"></script>
    <script src="js/xcharts.min.js"></script>
    <script src="js/jquery.autosize.min.js"></script>
    <script src="js/jquery.placeholder.min.js"></script>
    <script src="js/gdp-data.js"></script>
    <script src="js/morris.min.js"></script>
    <script src="js/sparklines.js"></script>
    <script src="js/charts.js"></script>
    <script src="js/jquery.slimscroll.min.js"></script>
    <script>
      //knob
      $(function() {
        $(".knob").knob({
          'draw': function() {
            $(this.i).val(this.cv + '%')
          }
        })
      });

      //carousel
      $(document).ready(function() {
        $("#owl-slider").owlCarousel({
          navigation: true,
          slideSpeed: 300,
          paginationSpeed: 400,
          singleItem: true

        });
      });

      //custom select box

      $(function() {
        $('select.styled').customSelect();
      });

      /* ---------- Map ---------- */
      $(function() {
        $('#map').vectorMap({
          map: 'world_mill_en',
          series: {
            regions: [{
              values: gdpData,
              scale: ['#000', '#000'],
              normalizeFunction: 'polynomial'
            }]
          },
          backgroundColor: '#eef3f7',
          onLabelShow: function(e, el, code) {
            el.html(el.html() + ' (GDP - ' + gdpData[code] + ')');
          }
        });
      });
    </script>

</body>

</html>
