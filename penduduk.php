<?php
$DB_NAME = "pendataan";
    $DB_USER = "root";
    $DB_PASS = "";
    $DB_SERVER_LOC = "localhost";

    $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
    $sql = "SELECT p.nik, p.nama, p.jenkel, p.pekerjaan, p.id_dusun, p.photos, d.nm_dusun, 
            CONCAT('http://192.168.43.60/pendataan/images/',photos) AS url, d.id_dusun, e.id_ekonomi
            FROM penduduk p, dusun d, ekonomi e
            WHERE p.id_dusun = d.id_dusun AND p.id_ekonomi = e.id_ekonomi" ;


    $result = mysqli_query($conn,$sql);
?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Pendataan Penduduk</title>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <a class="navbar-brand" href="index.php"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
            aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="index.php">Beranda<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active">
                   <a class="nav-link" href="penduduk.php">Data Penduduk</a>
                </li>
                <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Item
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="dusun.php">Data Dusun
                <a class="dropdown-item" href="ekonomi.php">Data Ekonomi</a>
                </div>
            </ul>
        </div>
    </nav>
    <br>
    <div class="container">
    <h1><p class="text-center">Data Penduduk</h1>
        <div class="table-responsive">
            <table class="table table-bordered table-striped bg-transparent">
                <thead class="table-striped table-primary" style="text-align: center;">
                    <tr>
                         <th scope="col">No</th>
                        <th scope="col">NIK</th>
                        <th scope="col">Nama Penduduk</th>
                        <th scope="col">Jenis Kelamin</th>
                        <th scope="col">Pekerjaan</th>
                        <th scope="col">Nama Dusun</th>
                        <th scope="col">Foto</th>
                        <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <?php
                    $no=1;
                    while($pdd = mysqli_fetch_assoc($result)){
                ?>
                <tbody>
                    <tr>
                        <th scope="row" style="text-align: center;"><?php echo $no;$no++; ?></th>
                        <td><?php echo $pdd['nik']; ?></td>
                        <td><?php echo $pdd['nama']; ?></td>
                        <td><?php echo $pdd['jenkel']; ?></td>
                        <td><?php echo $pdd['pekerjaan']; ?></td>
                        <td><?php echo $pdd['nm_dusun']; ?></td>
                        <td style="text-align: center;">
                            <img src="images/<?php echo $pdd['photos']; ?>" style="width: 200px;" alt="Foto Penduduk">


                            <td>
                                <a class="btn btn-success" href="editpenduduk.php?nik=<?php echo $pdd['nik']; ?> &aksi=edit_penduduk"><span class="icon_pencil_alt"></span>Edit</a>
                            <a class="btn btn-danger" href="proses.php?nik=<?php echo $pdd['nik']; ?> &aksi=hapus_penduduk"><span class="icon_trash_alt"></span>Hapus</a>
                            </td>
                            
                        </td>
                    </tr>
                    </tr>
                </tbody>
                <?php } ?>
            </table>
            <td><a class="btn btn-primary" href="tambahpenduduk.php">Tambah Penduduk</a></td>
            <br>
            <br>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
    </script>
</body>

</html>