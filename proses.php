<!doctype html>
<?php
    include 'koneksi.php';
    $db = new database();
    session_start();
?>
<?php
$aksi = $_GET['aksi'];
if($aksi == "tambah_penduduk")
{
	$db->input_penduduk($_POST['nik'],$_POST['nama'],$_POST['jenkel'],$_POST['pekerjaan'],$_POST['id_dusun'],$_POST['id_ekonomi'],$_POST['photos']);
    header("location:penduduk.php");
}
//hapus penduduk
elseif ($aksi =="hapus_penduduk")
{
    $db->delete_penduduk($_POST['nik']);
    header("location:penduduk.php");
}
// edit penduduk
elseif ($aksi =="edit_penduduk")
{
    $db->edit_penduduk($_POST['nama'],$_POST['jenkel'],$_POST['rating'],$_POST['npsn']);
    header("location:penduduk.php");
}
// update penduduk
elseif ($aksi =="update_penduduk")
{
    $db->update_penduduk($_POST['nik'],$_POST['nama'],$_POST['jenkel'],$_POST['pekerjaan'],$_POST['id_dusun'],$_POST['id_ekonomi'],$_POST['photos']);
    header("location:penduduk.php");
}
?>