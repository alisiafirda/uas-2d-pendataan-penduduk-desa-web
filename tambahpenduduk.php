<?php include 'koneksi.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="bootstrap.min.css" />
    <title>Tambah Penduduk</title>
</head>
<body>
    <div class="row justify-content-center">
        <div class="col-md-6 mt-3">
            <div class="card">
                <div class="card-body">
                    <h3>Tambah Data</h3>
                    <form action="proses.php?aksi=tambah_penduduk" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label>NIK</label>
                            <input name="nik" class="form-control" />    
                        </div> 
                        
                        <div class="form-group">
                            <label>Nama Penduduk</label>
                            <input name="nama" class="form-control" />    
                        </div> 
                        
                        <tr>
                        <td>Jenis Kelamin</td>
                        <td><select class="form-control" name="jenkel">
                                        <option value='Laki-Laki'>Laki-Laki</option>
                                        <option value='Perempuan'>Perempuan</option>
                             </select></td>
                    </tr>
                        <div class="form-group">
                            <labelm>Pekerjaan</label>              
                            <input name="pekerjaan" class="form-control" />
                        </div>
                        <div class="form-group">
                                    <label class="control-label" for="id_dusun">Dusun</label>
                                    <select class="form-control" name="id_dusun">
                                        <option value='1'>Gurah</option>
                                        <option value='2'>Gampeng</option>
                                        <option value='3'>Pare</option>
                                    </select>
                        </div>
                        <div class="form-group">
                                    <label class="control-label" for="id_ekonomi">Ekonomi</label>
                                    <select class="form-control" name="id_ekonomi">
                                        <option value='e1'>Atas</option>
                                        <option value='e2'>Menengah</option>
                                         <option value='e3'>Bawah</option>
                                    </select>
                        </div>
                        <div class="form-group">
                            <label>Foto</label>
                            <input type="file" class="form-control" name="photos" />
                        </div>   
                        <button type="submit" class="btn btn-primary">Simpan</button> 
                        <a href="penduduk.php" class="btn btn-success">Batal</a>             
                    </form>
                </div>
            </div>
        </div>
    </div>  
</body>
</html>